package fitnessapp.backend.controller;

import fitnessapp.backend.mapper.UserMapper;
import fitnessapp.backend.model.dto.MacroNutrientsDto;
import fitnessapp.backend.model.dto.UserDto;
import fitnessapp.backend.service.MacroCalculateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class CalculateController {

    @Autowired
    private MacroCalculateService macroCalculateService;

    @Autowired
    private UserMapper userMapper;

    @PostMapping("/tdee")
    public MacroNutrientsDto calculateTdee(@Validated @RequestBody UserDto user) {
        macroCalculateService.saveUser(userMapper.convertUserDtoToDao(user));
        return macroCalculateService.calculateBmr(user);
    }
}
