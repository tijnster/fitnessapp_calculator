package fitnessapp.backend.mapper;

import fitnessapp.backend.model.dao.MacronutrientsDao;
import fitnessapp.backend.model.dto.MacroNutrientsDto;
import org.springframework.stereotype.Service;

@Service
public class MacronutrientsMapper {

    public MacronutrientsDao convertMacronutrientsDtoToDao(MacroNutrientsDto macroNutrientsDto) {
        MacronutrientsDao macronutrientsDao = new MacronutrientsDao();

        macronutrientsDao.setBmr(macroNutrientsDto.getBmr());
        macronutrientsDao.setGramOfProteines(macroNutrientsDto.getAmountOfProteines());
        macronutrientsDao.setGramOfCarbs(macroNutrientsDto.getAmountOfCarbs());
        macronutrientsDao.setGramOfFats(macroNutrientsDto.getAmountOfFats());
        macronutrientsDao.setPercentageOfProteines(macroNutrientsDto.getPercentageProteins());
        macronutrientsDao.setPercentageOfFats(macroNutrientsDto.getPercentageFats());
        macronutrientsDao.setPercentageOfCarbs(macroNutrientsDto.getPercentageCarbs());
        macronutrientsDao.setEnergyOfProteins(macroNutrientsDto.getProteinEnergy());
        macronutrientsDao.setEnergyOfCarbs(macroNutrientsDto.getCarbEnergy());
        macronutrientsDao.setEnergyOfFats(macroNutrientsDto.getFatEnergy());

        return macronutrientsDao;
    }
}
