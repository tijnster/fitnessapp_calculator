package fitnessapp.backend.mapper;

import fitnessapp.backend.model.dao.UserDao;
import fitnessapp.backend.model.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public class UserMapper {

    public UserDao convertUserDtoToDao(UserDto user) {
        UserDao userDao = new UserDao();
        userDao.setName(user.getName());
        userDao.setGender(user.getGender());
        userDao.setWeight(user.getWeight());
        userDao.setLength(user.getLength());
        userDao.setAge(user.getAge());
        userDao.setActivityLevel(user.getActivity());
        userDao.setGoal(user.getGoal());

        return userDao;
    }
}
