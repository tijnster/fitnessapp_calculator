package fitnessapp.backend.model.dao;

import lombok.Data;
import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table (name="MACRONUTRIENTS")
public class MacronutrientsDao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @Column(name="BMR")
    private int bmr;

    @Column(name="GRAM_PROTEINES")
    private int gramOfProteines;

    @Column(name="GRAM_CARBS")
    private int gramOfCarbs;

    @Column(name="GRAM_FATS")
    private int gramOfFats;

    @Column(name="PERCENTAGE_PROTEINES")
    private int percentageOfProteines;

    @Column(name="PERCENTAGE_FATS")
    private int percentageOfFats;

    @Column(name="PERCENTAGE_CARBS")
    private int percentageOfCarbs;

    @Column(name="ENERGY_PROTEINES")
    private int energyOfProteins;

    @Column(name="ENERGY_CARBS")
    private int energyOfCarbs;

    @Column(name="ENERGY_FATS")
    private int energyOfFats;

    @OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL},orphanRemoval=true)
    @JoinColumn(name="MACRO_ID")
    private List <UserDao> user;
}
