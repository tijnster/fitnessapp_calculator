package fitnessapp.backend.model.dao;

import com.sun.istack.NotNull;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "USER")
public class UserDao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column (name = "NAME")
    private String name;

    @NotNull
    @Column (name = " GENDER")
    private String gender;

    @NotNull
    @Column (name = "WEIGHT")
    private int weight;

    @NotNull
    @Column (name = "LENGTH")
    private int length;

    @NotNull
    @Column (name = "AGE")
    private int age;

    @NotNull
    @Column (name = "ACTIVITY_LEVEL")
    private double activityLevel;

    @NotNull
    @Column (name = "GOAL")
    private String goal;
}
