package fitnessapp.backend.model.dto;

import lombok.Data;

@Data
public class MacroNutrientsDto {

    private int bmr;
    private int amountOfProteines;
    private int amountOfCarbs;
    private int amountOfFats;
    private int percentageProteins = 40;
    private int percentageCarbs = 40;
    private int percentageFats = 20;
    private int carbEnergy;
    private int proteinEnergy;
    private int fatEnergy;
}
