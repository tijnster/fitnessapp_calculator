package fitnessapp.backend.model.dto;

import lombok.Data;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Data
public class UserDto {

    private long Id;
    private String name;
    private String gender;
    private int weight;
    private int length;
    private int age;
    private double activity;
    private String goal;

}
