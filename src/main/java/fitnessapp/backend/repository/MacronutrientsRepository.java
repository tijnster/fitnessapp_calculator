package fitnessapp.backend.repository;

import fitnessapp.backend.model.dao.MacronutrientsDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MacronutrientsRepository extends JpaRepository<MacronutrientsDao, Long> {
}
