package fitnessapp.backend.service;

import fitnessapp.backend.mapper.MacronutrientsMapper;
import fitnessapp.backend.model.dao.UserDao;
import fitnessapp.backend.model.dto.MacroNutrientsDto;
import fitnessapp.backend.model.dto.UserDto;
import fitnessapp.backend.repository.MacronutrientsRepository;
import fitnessapp.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MacroCalculateService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MacronutrientsRepository macronutrientsRepository;

    @Autowired
    private MacronutrientsMapper macronutrientsMapper;

    public void saveUser(UserDao user) {
        userRepository.save(user);
    }

    public MacroNutrientsDto calculateBmr(UserDto user) {

        MacroNutrientsDto macroNutrientsDto = new MacroNutrientsDto();
        double calcBmr;

        if(user.getGender().equals("man")) {
            calcBmr = 10 * user.getWeight() + 6.25 * user.getLength() - 5 * user.getAge() + 5;
        } else {
            calcBmr = 10 * user.getWeight() + 6.25 * user.getLength() - 5 * user.getAge() - 161;
        }

        macroNutrientsDto.setBmr((int) calcBmr);

        // calculate energy's
        double proteines = macroNutrientsDto.getBmr() * 0.4;
        macroNutrientsDto.setProteinEnergy((int) proteines);

        double carbs = macroNutrientsDto.getBmr() * 0.4;
        macroNutrientsDto.setCarbEnergy((int) carbs);

        double fats = macroNutrientsDto.getBmr() * 0.2;
        macroNutrientsDto.setFatEnergy((int) fats);

        //calculate exact amounts
        macroNutrientsDto.setAmountOfFats((int) macroNutrientsDto.getFatEnergy() / 9);
        macroNutrientsDto.setAmountOfCarbs((int) macroNutrientsDto.getCarbEnergy() / 4);
        macroNutrientsDto.setAmountOfProteines((int) macroNutrientsDto.getProteinEnergy() / 4);

        macronutrientsRepository.save(macronutrientsMapper.convertMacronutrientsDtoToDao(macroNutrientsDto));

        return macroNutrientsDto;
    }
}
